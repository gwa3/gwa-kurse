package de.vahrst.gwa.kurse.domain.model;

import de.vahrst.gwa.kurse.domain.ports.KurseRepository;
import de.vahrst.gwa.kurse.domain.ports.types.KursTyp;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Junit-Test der Klasse {@link KursverwaltungServiceImpl}
 */
class KursverwaltungServiceImplTest {

    private KursverwaltungServiceImpl kursverwaltungService;
    private KurseRepository kurseRepository;

    @BeforeEach
    public void setup(){
        kurseRepository = mock(KurseRepository.class);
        kursverwaltungService = new KursverwaltungServiceImpl(kurseRepository);

        List<KursTyp> dbResult = List.of(
                new KursTyp(UUID.fromString("7ed9a13a-5a2e-480d-a61f-019553a36fe8"),
                        "Indoor-Cycling",
                        "Indoor-Cycling",
                        LocalDate.parse("9999-12-31")),

                new KursTyp(UUID.fromString("dbe9a5de-c8fb-44c3-9ce8-71158f231506"),
                        "Schlingentraining",
                        "Schlingentraining",
                        LocalDate.parse("9999-12-31")),

                new KursTyp(UUID.fromString("caad6a38-8adb-4332-8151-8932157c3704"),
                        "Alter Kurs",
                        "bis heute gültig, morgen nicht mehr",
                        LocalDate.now())
        );
        when(kurseRepository.getKursTypen()).thenReturn(dbResult);
    }

    @Test
    public void getKursTypen_shouldReturnValidKursTypenForDate(){
        LocalDate heute = LocalDate.now();
        LocalDate gestern = heute.minusDays(1);
        LocalDate morgen = heute.plusDays(1);

        assertThat(kursverwaltungService.getKursTypen(gestern)).hasSize(3);
        assertThat(kursverwaltungService.getKursTypen(heute)).hasSize(3);
        assertThat(kursverwaltungService.getKursTypen(morgen)).hasSize(2);

    }

    @Test
    public void getKursTypen_shouldThrowExceptionOnNullDateParameter(){
        assertThatThrownBy(() -> {
            kursverwaltungService.getKursTypen(null);
        }).isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Datum-Parameter darf nicht null sein!");
    }
}