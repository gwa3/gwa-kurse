package de.vahrst.gwa.kurse.adapters.api;

import de.vahrst.gwa.kurse.adapters.api.types.KursTypDTO;
import de.vahrst.gwa.kurse.domain.ports.types.KursTyp;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

/**
 * IntegrationTests der Anwendung über die Rest-Endpunkte.
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("dev")
@Testcontainers
public class ControllerITest {

    @Container
    private static PostgreSQLContainer postgreSQLContainer = new PostgreSQLContainer("postgres:latest");

    @DynamicPropertySource
    static void registerPgProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.username", postgreSQLContainer::getUsername);
        registry.add("spring.datasource.password", postgreSQLContainer::getPassword);
        registry.add("spring.datasource.url", postgreSQLContainer::getJdbcUrl);
    }

    @LocalServerPort
    private int port;

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    private RestTemplateBuilder restTemplateBuilder;

    @Test
    public void getKursTyp_200(){
        RestTemplate rt = restTemplateBuilder.build();

        ResponseEntity<KursTypDTO> result = rt.exchange("http://localhost:" + port + "/gwa/api/stammdaten/kurstypen/7ed9a13a-5a2e-480d-a61f-019553a36fe8",
                HttpMethod.GET,
                null,
                KursTypDTO.class
                );

        assertThat(result.getStatusCode().value()).isEqualTo(200);
        KursTypDTO dto = result.getBody();

        // Das ist eine der IDs aus den Testdaten:
        assertThat(dto.getName()).isEqualTo("Indoor-Cycling");
    }

    @Test
    public void getKursTyp_400(){
        RestTemplate rt = restTemplateBuilder.build();

        assertThatThrownBy(() -> {
            ResponseEntity<KursTypDTO> result = rt.exchange("http://localhost:" + port + "/gwa/api/stammdaten/kurstypen/ffffffff-5a2e-480d-a61f-019553a36fe8",
                    HttpMethod.GET,
                    null,
                    KursTypDTO.class
            );
        }).isInstanceOf(HttpClientErrorException.NotFound.class);
    }

    @Test
    public void getKursTyp_4xx(){
        RestTemplate rt = restTemplateBuilder.build();

        // trullala ist keine UUID
        assertThatThrownBy(() -> {
            ResponseEntity<KursTypDTO> result = rt.exchange("http://localhost:" + port + "/gwa/api/stammdaten/kurstypen/trullala",
                HttpMethod.GET,
                null,
                KursTypDTO.class
            );
        }).isInstanceOf(HttpClientErrorException.class);
        // ich hätte 400 erwartet, aber Spring webmvc schickt 403 forbidden zurück, wenn der parameter nicht mappt.
        // daher hier der Check auf die allgemeingültigere Exception.
    }


    @Test
    public void getKursTypen(){
        RestTemplate rt = restTemplateBuilder.build();

        ResponseEntity<List<KursTypDTO>> result = rt.exchange("http://localhost:" + port + "/gwa/api/stammdaten/kurstypen",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<KursTypDTO>>() {
                });


        List<KursTypDTO> kurstypen = result.getBody();
        // Das ist eine der IDs aus den Testdaten:
        assertThat(kurstypen).extracting(KursTypDTO::getId).contains("7ed9a13a-5a2e-480d-a61f-019553a36fe8", "dbe9a5de-c8fb-44c3-9ce8-71158f231506");
    }

    @Test
    public void postKursTyp() throws URISyntaxException {
        RestTemplate rt = restTemplateBuilder.build();
        KursTypDTO dto = KursTypDTO.fromKursTyp(new KursTyp(null, "Neuer Kurs", "Trullalal", LocalDate.parse("9999-12-31")));

        ResponseEntity<KursTypDTO> responseEntity = rt.postForEntity("http://localhost:" + port + "/gwa/api/stammdaten/kurstypen", dto, KursTypDTO.class);

        String newID = responseEntity.getBody().getId();

        assertThat(responseEntity.getHeaders().getLocation()).isEqualTo(new URI("http://localhost:" + port + "/gwa/api/stammdaten/kurstypen/" + newID));

    }

    @Test
    public void patchKursTyp() throws URISyntaxException {
        RestTemplate rt = restTemplateBuilder.build();

        // Wir legen erstmal einen neuen Kurstyp an, den wir dann ändern.
        KursTypDTO dto = KursTypDTO.fromKursTyp(new KursTyp(null, "Neuer KursTyp", "Noch ein Test", LocalDate.parse("9999-12-31")));

        ResponseEntity<KursTypDTO> responseEntity = rt.postForEntity("http://localhost:" + port + "/gwa/api/stammdaten/kurstypen", dto, KursTypDTO.class);

        String newID = responseEntity.getBody().getId();

        KursTypDTO updateDto = KursTypDTO.fromKursTyp(new KursTyp(UUID.fromString(newID), "Neuer KursTüüp", "Geändert", LocalDate.parse("9999-12-31")));
        ResponseEntity<KursTypDTO> response2 = rt.exchange("http://localhost:" + port + "/gwa/api/stammdaten/kurstypen/" + newID, HttpMethod.PATCH, new HttpEntity<>(updateDto), KursTypDTO.class);
        assertThat(response2.getStatusCode().value()).isEqualTo(200);
        assertThat(response2.getBody().toKursTyp()).isEqualTo(updateDto.toKursTyp());
    }

    @Test
    public void patchKursTyp_differentIds() throws URISyntaxException {
        RestTemplate rt = restTemplateBuilder.build();

        String uuid1 = "7ed9a13a-5a2e-480d-a61f-019553a36fe8";
        String uuid2 = "ffffffff-5a2e-480d-a61f-019553a36fe8";

        assertThatThrownBy(() -> {
            KursTypDTO updateDto = KursTypDTO.fromKursTyp(new KursTyp(UUID.fromString(uuid1), "Neuer KursTüüp", "Geändert", LocalDate.parse("9999-12-31")));
            ResponseEntity<KursTypDTO> response2 = rt.exchange("http://localhost:" + port + "/gwa/api/stammdaten/kurstypen/" + uuid2, HttpMethod.PATCH, new HttpEntity<>(updateDto), KursTypDTO.class);

        }).isInstanceOf(HttpClientErrorException.BadRequest.class);
    }

    @Test
    public void patchKursTyp_wrongId() throws URISyntaxException {
        RestTemplate rt = restTemplateBuilder.build();

        String uuid = "ffffffff-5a2e-480d-a61f-019553a36fe8";

        assertThatThrownBy(() -> {
            KursTypDTO updateDto = KursTypDTO.fromKursTyp(new KursTyp(UUID.fromString(uuid), "Neuer KursTüüp", "Geändert", LocalDate.parse("9999-12-31")));
            ResponseEntity<KursTypDTO> response2 = rt.exchange("http://localhost:" + port + "/gwa/api/stammdaten/kurstypen/" + uuid, HttpMethod.PATCH, new HttpEntity<>(updateDto), KursTypDTO.class);

        }).isInstanceOf(HttpClientErrorException.NotFound.class);
    }
}
