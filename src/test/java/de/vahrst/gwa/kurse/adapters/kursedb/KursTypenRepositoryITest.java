package de.vahrst.gwa.kurse.adapters.kursedb;

import com.github.database.rider.core.api.connection.ConnectionHolder;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.api.dataset.ExpectedDataSet;
import com.github.database.rider.junit5.api.DBRider;
import de.vahrst.gwa.kurse.domain.ports.types.KursTyp;
import org.flywaydb.core.Flyway;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.postgresql.ds.PGSimpleDataSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test den KursTypenRepositories mit TestContainers und DB-Raider
 * aber ohne Spring
 */
@Testcontainers
@DBRider
class KursTypenRepositoryITest {

    @Container
    private static PostgreSQLContainer postgreSQLContainer = new PostgreSQLContainer("postgres:latest");

    private static PGSimpleDataSource dataSource;

    private KursTypenRepository kursTypenRepository;

    // wird von DBRider verwendet:
    private ConnectionHolder connectionHolder = () -> dataSource.getConnection();


    @BeforeAll
    public static void setupDataSource(){
        dataSource = new PGSimpleDataSource();
        dataSource.setServerNames(new String[]{postgreSQLContainer.getHost()});
        dataSource.setPortNumbers(new int[]{postgreSQLContainer.getFirstMappedPort()});
        dataSource.setDatabaseName(postgreSQLContainer.getDatabaseName());
        dataSource.setUser(postgreSQLContainer.getUsername());
        dataSource.setPassword(postgreSQLContainer.getPassword());

        Flyway flyway = Flyway.configure()
                .dataSource(dataSource)
                .locations("classpath:/db/migration/shared", "classpath:/db/migration/non-prod")
                .load();
        flyway.migrate();

    }

    @BeforeEach
    public void setup(){
        NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        kursTypenRepository = new KursTypenRepository(jdbcTemplate);
    }

    @Test
    @DataSet(value="datasets/init_kurstypen.yml")
    public void getKursTypen_shouldReturnAllItems(){

        List<KursTyp> kursTypen = kursTypenRepository.getKursTypen();
        assertThat(kursTypen).hasSize(3);
    }

    @Test
    @DataSet(value="datasets/init_kurstypen.yml")
    public void getKursTypById_shouldReturnKursTyp(){

        KursTyp kursTyp = kursTypenRepository.getKursTyp(UUID.fromString("dbe9a5de-c8fb-44c3-9ce8-71158f231506"), LocalDate.parse("2022-12-28"));
        assertThat(kursTyp.name()).isEqualTo("Schlingentraining");
    }

    @Test
    @DataSet(value="datasets/init_kurstypen.yml")
    public void getKursTypById_shouldReturnNull(){
        // Zugriff mit falscher ID und mit richtiger ID + Datum zu spät

        // Das muss noch gehen:
        KursTyp kursTyp = kursTypenRepository.getKursTyp(UUID.fromString("caad6a38-8adb-4332-8151-8932157c3704"), LocalDate.parse("2022-12-01"));
        assertThat(kursTyp.name()).isEqualTo("Alter Kurs");

        // Das darf nicht mehr gehen:
        kursTyp = kursTypenRepository.getKursTyp(UUID.fromString("caad6a38-8adb-4332-8151-8932157c3704"), LocalDate.parse("2022-12-02"));
        assertThat(kursTyp).isNull();

        // und mit falscher ID muss auch null kommen:
        // Das darf nicht mehr gehen:
        kursTyp = kursTypenRepository.getKursTyp(UUID.fromString("ffffffff-8adb-4332-8151-8932157c3704"), LocalDate.parse("2000-01-01"));
        assertThat(kursTyp).isNull();
    }


    @Test
    @DataSet(value="datasets/init_kurstypen.yml")
    @ExpectedDataSet(value="datasets/expected_kurstypen_1.yml", ignoreCols = "id", orderBy = "name")
    public void insertKursTypen_shouldPersistNewItem(){
        KursTyp input = new KursTyp(null, "Sonstiges", "Sonstiger Kurs", LocalDate.parse("2022-12-27"));
        KursTyp newKursTyp = kursTypenRepository.insertKursTyp(input);
        assertThat(newKursTyp.id()).isNotNull();
        assertThat(newKursTyp.name()).isEqualTo(input.name());
        assertThat(newKursTyp.beschreibung()).isEqualTo(input.beschreibung());
        assertThat(newKursTyp.gueltigBis()).isEqualTo(input.gueltigBis());

        List<KursTyp> kursTypen = kursTypenRepository.getKursTypen();
        assertThat(kursTypen).contains(newKursTyp);

    }

    @Test
    @DataSet(value="datasets/init_kurstypen.yml")
    @ExpectedDataSet(value="datasets/expected_kurstypen_2.yml", orderBy = "name")
    public void updateKursTypen_shouldPersistNewItem(){
        KursTyp kursTyp = kursTypenRepository.getKursTyp(UUID.fromString("dbe9a5de-c8fb-44c3-9ce8-71158f231506"), LocalDate.parse("2022-12-28"));
        assertThat(kursTyp.name()).isEqualTo("Schlingentraining");

        KursTyp input = new KursTyp(kursTyp.id(), "Schlingentraining Update", "Trullala", LocalDate.parse("9999-12-01"));
        KursTyp result = kursTypenRepository.updateKursTyp(input);
        assertThat(result).isEqualTo(input);
    }
}