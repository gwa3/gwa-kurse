package de.vahrst.gwa.kurse.adapters.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.vahrst.gwa.kurse.adapters.api.security.WebSecurityConfiguration;
import de.vahrst.gwa.kurse.adapters.api.types.KursTypDTO;
import de.vahrst.gwa.kurse.domain.ports.KursverwaltungService;
import de.vahrst.gwa.kurse.domain.ports.types.KursTyp;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(KursTypenController.class)
@Import(WebSecurityConfiguration.class)
public class KursTypenControllerWebMvcTest {
    private static final Logger logger = LoggerFactory.getLogger(KursTypenControllerWebMvcTest.class);

    @MockBean
    private KursverwaltungService kursverwaltungService;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private Jackson2ObjectMapperBuilder objectMapperBuilder;


    @Test
    public void getKursTypen_ShouldReturnKursTypDTOs() throws Exception {
        List<KursTyp> kursTypen = List.of(
                new KursTyp(UUID.fromString("7ed9a13a-5a2e-480d-a61f-019553a36fe8"),
                        "Indoor-Cycling",
                        "Indoor-Cycling",
                        LocalDate.parse("9999-12-31")),

                new KursTyp(UUID.fromString("dbe9a5de-c8fb-44c3-9ce8-71158f231506"),
                        "Schlingentraining",
                        "Schlingentraining",
                        LocalDate.parse("9999-12-31"))

        );
        LocalDate datum = LocalDate.parse("2022-12-27");
        when(kursverwaltungService.getKursTypen(datum)).thenReturn(kursTypen);


        MvcResult mvcResult = mockMvc.perform(get("/api/stammdaten/kurstypen?datum=2022-12-27"))
                .andExpect(status().isOk())
                .andReturn();

        String contentAsString = mvcResult.getResponse().getContentAsString();
//        logger.info(contentAsString);

        String expected = """
                [
                  {
                    "id": "7ed9a13a-5a2e-480d-a61f-019553a36fe8",
                    "name": "Indoor-Cycling",
                    "bezeichnung": "Indoor-Cycling",
                    "gueltigBis": "9999-12-31"
                  },
                  {
                    "id": "dbe9a5de-c8fb-44c3-9ce8-71158f231506",
                    "name": "Schlingentraining",
                    "bezeichnung": "Schlingentraining",
                    "gueltigBis": "9999-12-31"
                  }
                ]
                """;

        JSONAssert.assertEquals(expected, contentAsString, true);
    }


    @Test
    public void getKursTyp_ShouldReturnKursTypDTO() throws Exception {
        UUID id = UUID.fromString("7ed9a13a-5a2e-480d-a61f-019553a36fe8");
        KursTyp kursTyp = new KursTyp(id,
                "Indoor-Cycling",
                "Indoor-Cycling",
                LocalDate.parse("9999-12-31"));


        LocalDate datum = LocalDate.parse("2022-12-27");
        when(kursverwaltungService.getKursTyp(id, datum)).thenReturn(kursTyp);


        MvcResult mvcResult = mockMvc.perform(get("/api/stammdaten/kurstypen/" + id.toString() + "?datum=2022-12-27"))
                .andExpect(status().isOk())
                .andReturn();

        String contentAsString = mvcResult.getResponse().getContentAsString();
//        logger.info(contentAsString);

        String expected = """
                  {
                    "id": "7ed9a13a-5a2e-480d-a61f-019553a36fe8",
                    "name": "Indoor-Cycling",
                    "bezeichnung": "Indoor-Cycling",
                    "gueltigBis": "9999-12-31"
                  }
                """;
        JSONAssert.assertEquals(expected, contentAsString, true);
    }

    @Test
    public void getKursTyp_ShouldReturn404() throws Exception {
        UUID id = UUID.fromString("7ed9a13a-5a2e-480d-a61f-019553a36fe8");

        LocalDate datum = LocalDate.parse("2022-12-27");
        when(kursverwaltungService.getKursTyp(id, datum)).thenReturn(null);


        MvcResult mvcResult = mockMvc.perform(get("/api/stammdaten/kurstypen/" + id.toString() + "?datum=2022-12-27"))
                .andExpect(status().isNotFound())
                .andReturn();
    }


    @Test
    public void kurstypen_post_shouldReturnDTOAndLocation() throws Exception {
        UUID uuid = UUID.fromString("a50e37c9-dce6-42c5-b755-aa535268d156");

        when(kursverwaltungService.addKursTyp(any(KursTyp.class))).thenAnswer(i -> {
            KursTyp input = (KursTyp) i.getArgument(0);
            return new KursTyp(uuid, input.name(), input.beschreibung(), input.gueltigBis());
        });

        String inputdata = """
                {
                  "name": "Test",
                  "bezeichnung": "Trullalal",
                  "gueltigBis": "2025-01-01"
                }""";

        MvcResult mvcResult = mockMvc.perform(post("/api/stammdaten/kurstypen")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(inputdata))
                .andExpect(status().isCreated())
                .andExpect(header().string("location", "http://localhost/api/stammdaten/kurstypen/" + uuid))
                .andReturn();

        String contentAsString = mvcResult.getResponse().getContentAsString();

        ObjectMapper om = objectMapperBuilder.build();

        KursTypDTO kursTypDTO = om.readValue(contentAsString, KursTypDTO.class);

        assertThat(kursTypDTO.getId()).isEqualTo(uuid.toString());
    }

    @Test
    public void kurstypen_patch_shouldReturn200() throws Exception {
        UUID uuid = UUID.fromString("a50e37c9-dce6-42c5-b755-aa535268d156");

        when(kursverwaltungService.updateKursTyp(any(KursTyp.class))).thenAnswer(i -> i.getArgument(0));


        String inputdata = """
                {
                  "id": "a50e37c9-dce6-42c5-b755-aa535268d156",
                  "name": "Test",
                  "bezeichnung": "Trullalal",
                  "gueltigBis": "2025-01-01"
                }""";

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.patch("/api/stammdaten/kurstypen/" + uuid)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(inputdata))
                .andExpect(status().isOk())
                .andReturn();

    }

    @Test
    public void kurstypen_patch_differenIds_shouldReturn400() throws Exception {
        UUID uuid1 = UUID.fromString("a50e37c9-dce6-42c5-b755-aa535268d156");
        UUID uuid2 = UUID.fromString("ffffffff-dce6-42c5-b755-aa535268d156");

        when(kursverwaltungService.updateKursTyp(any(KursTyp.class))).thenAnswer(i -> i.getArgument(0));


        String inputdata = """
                {
                  "id": "a50e37c9-dce6-42c5-b755-aa535268d156",
                  "name": "Test",
                  "bezeichnung": "Trullalal",
                  "gueltigBis": "2025-01-01"
                }""";

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.patch("/api/stammdaten/kurstypen/" + uuid2)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(inputdata))
                .andExpect(status().is(400))
                .andReturn();

        String responseBody = mvcResult.getResponse().getContentAsString();
        System.out.println(responseBody);

    }

    @Test
    public void kurstypen_patch_wrongId_shouldReturn404() throws Exception {
        UUID uuid = UUID.fromString("a50e37c9-dce6-42c5-b755-aa535268d156");

        when(kursverwaltungService.updateKursTyp(any(KursTyp.class))).thenReturn(null);


        String inputdata = """
                {
                  "id": "a50e37c9-dce6-42c5-b755-aa535268d156",
                  "name": "Test",
                  "bezeichnung": "Trullalal",
                  "gueltigBis": "2025-01-01"
                }""";

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.patch("/api/stammdaten/kurstypen/" + uuid)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(inputdata))
                .andExpect(status().is(404))
                .andReturn();


    }

}
