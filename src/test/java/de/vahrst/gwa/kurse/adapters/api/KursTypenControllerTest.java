package de.vahrst.gwa.kurse.adapters.api;

import de.vahrst.gwa.kurse.adapters.api.types.KursTypDTO;
import de.vahrst.gwa.kurse.domain.ports.KursverwaltungService;
import de.vahrst.gwa.kurse.domain.ports.types.KursTyp;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Einfacher JUnit-Test (ohne Spring und Domainen-Klassen)
 */
class KursTypenControllerTest {

    private KursTypenController kursTypenController;
    private KursverwaltungService kursverwaltungService;

    @BeforeEach
    public void setup(){
        kursverwaltungService = mock(KursverwaltungService.class);
        kursTypenController = new KursTypenController(kursverwaltungService);
    }

    @Test
    public void getKursTypen_ShouldReturnKursTypDTOListe(){
        List<KursTyp> kursTypen = List.of(
                new KursTyp(UUID.fromString("7ed9a13a-5a2e-480d-a61f-019553a36fe8"),
                        "Indoor-Cycling",
                        "Indoor-Cycling",
                        LocalDate.parse("9999-12-31")),

                new KursTyp(UUID.fromString("dbe9a5de-c8fb-44c3-9ce8-71158f231506"),
                        "Schlingentraining",
                        "Schlingentraining",
                        LocalDate.parse("9999-12-31"))

        );
        LocalDate datum = LocalDate.parse("2022-12-10");
        when(kursverwaltungService.getKursTypen(datum)).thenReturn(kursTypen);

        List<KursTypDTO> kursTypDTOs = kursTypenController.getKursTypen(datum);
        assertThat(kursTypDTOs).hasSize(2);
        assertThat(kursTypDTOs.get(0).toKursTyp()).isEqualTo(kursTypen.get(0));
        assertThat(kursTypDTOs.get(1).toKursTyp()).isEqualTo(kursTypen.get(1));


    }

    @Test
    public void getKursTyp_ShouldReturnKursTypDTO(){
        UUID id = UUID.fromString("7ed9a13a-5a2e-480d-a61f-019553a36fe8");
        KursTyp kursTyp = new KursTyp(id,
                "Indoor-Cycling",
                "Indoor-Cycling",
                LocalDate.parse("9999-12-31"));

        LocalDate datum = LocalDate.parse("2022-12-10");
        when(kursverwaltungService.getKursTyp(id, datum)).thenReturn(kursTyp);

        ResponseEntity<KursTypDTO> response = kursTypenController.getKursTyp(id, datum);
        assertThat(response.getStatusCode().value()).isEqualTo(200);
        KursTypDTO dto = response.getBody();
        assertThat(dto).isNotNull();
        assertThat(dto.toKursTyp()).isEqualTo(kursTyp);
    }

    @Test
    public void getKursTyp_ShouldReturn404(){
        UUID id = UUID.fromString("7ed9a13a-5a2e-480d-a61f-019553a36fe8");

        LocalDate datum = LocalDate.parse("2022-12-10");
        when(kursverwaltungService.getKursTyp(id, datum)).thenReturn(null);

        ResponseEntity<KursTypDTO> response = kursTypenController.getKursTyp(id, datum);
        assertThat(response.getStatusCode().value()).isEqualTo(404);
    }


    @Test
    public void addKursTyp_ShouldReturnNewKursTypAndLocation() throws URISyntaxException {
        UUID uuid = UUID.fromString("a50e37c9-dce6-42c5-b755-aa535268d156");

        when(kursverwaltungService.addKursTyp(any(KursTyp.class))).thenAnswer(i -> {
            KursTyp input = (KursTyp) i.getArgument(0);
            return new KursTyp(uuid, input.name(), input.beschreibung(), input.gueltigBis());
        });

        KursTyp newKursTyp = new KursTyp(null, "Test", "Trullalal", LocalDate.parse("2022-10-05"));

        UriComponentsBuilder b = UriComponentsBuilder.fromHttpUrl("http://localhost:8081/rootcontext");
        ResponseEntity<KursTypDTO> responseEntity = kursTypenController.addKurstyp(KursTypDTO.fromKursTyp(newKursTyp), b);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(201));
        KursTypDTO responseDTO = responseEntity.getBody();
        assertThat(responseDTO.getId()).isEqualTo(uuid.toString());

        URI location = responseEntity.getHeaders().getLocation();
        System.out.println(location);
        assertThat(location).isEqualTo(new URI("http://localhost:8081/rootcontext/api/stammdaten/kurstypen/" + uuid.toString()));

    }

    @Test
    public void updateKursTyp_ShouldReturn200() throws URISyntaxException {
        UUID uuid = UUID.fromString("a50e37c9-dce6-42c5-b755-aa535268d156");

        when(kursverwaltungService.updateKursTyp(any(KursTyp.class))).thenAnswer(i -> i.getArgument(0));

        KursTyp updateKursTyp = new KursTyp(uuid, "Test", "Trullalal", LocalDate.parse("2022-10-05"));

        ResponseEntity<KursTypDTO> responseEntity = kursTypenController.updateKurstyp(KursTypDTO.fromKursTyp(updateKursTyp), uuid);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(200));
        KursTypDTO responseDTO = responseEntity.getBody();
        assertThat(responseDTO.toKursTyp()).isEqualTo(updateKursTyp);
    }

    @Test
    public void updateKursTyp_DifferentIds_ShouldReturn400() throws URISyntaxException {
        UUID uuid1 = UUID.fromString("a50e37c9-dce6-42c5-b755-aa535268d156");
        UUID uuid2 = UUID.fromString("ffffffff-dce6-42c5-b755-aa535268d156");

        when(kursverwaltungService.updateKursTyp(any(KursTyp.class))).thenAnswer(i -> i.getArgument(0));

        KursTyp updateKursTyp = new KursTyp(uuid1, "Test", "Trullalal", LocalDate.parse("2022-10-05"));

        ResponseEntity<?> responseEntity = kursTypenController.updateKurstyp(KursTypDTO.fromKursTyp(updateKursTyp), uuid2);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(400));
        System.out.println(responseEntity.getBody().toString());
    }

    @Test
    public void updateKursTyp_wrongId_ShouldReturn404() throws URISyntaxException {
        UUID uuid = UUID.fromString("ffffffff-dce6-42c5-b755-aa535268d156");

        when(kursverwaltungService.updateKursTyp(any(KursTyp.class))).thenAnswer(i -> i.getArgument(0));

        KursTyp updateKursTyp = new KursTyp(uuid, "Test", "Trullalal", LocalDate.parse("2022-10-05"));

        ResponseEntity<?> responseEntity = kursTypenController.updateKurstyp(KursTypDTO.fromKursTyp(updateKursTyp), uuid);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(404));
    }


}