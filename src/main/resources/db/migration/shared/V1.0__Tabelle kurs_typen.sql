create table kurs_typen(
  id UUID primary key , -- technische ID des Eintrags
  name varchar(64) not null,
  beschreibung varchar(2048) not null,
  gueltig_bis date not null
);