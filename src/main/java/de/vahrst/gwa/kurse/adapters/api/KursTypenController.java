package de.vahrst.gwa.kurse.adapters.api;

import de.vahrst.gwa.kurse.adapters.api.types.KursTypDTO;
import de.vahrst.gwa.kurse.domain.ports.KursverwaltungService;
import de.vahrst.gwa.kurse.domain.ports.types.KursTyp;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ProblemDetail;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@RestController
@Tag(name = "stammdaten")
public class KursTypenController {

    private final KursverwaltungService kursverwaltungService;

    public KursTypenController(KursverwaltungService kursverwaltungService) {
        this.kursverwaltungService = kursverwaltungService;
    }

    @GetMapping(value = "/api/stammdaten/kurstypen", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<KursTypDTO> getKursTypen(@RequestParam(value = "datum", required = false) LocalDate datum) {
        if (datum == null) {
            datum = LocalDate.now();
        }
        List<KursTyp> kursTypen = kursverwaltungService.getKursTypen(datum);
        return kursTypen.stream()
                .map(KursTypDTO::fromKursTyp)
                .toList();
    }

    @GetMapping(value="/api/stammdaten/kurstypen/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<KursTypDTO> getKursTyp(@PathVariable UUID id, @RequestParam(value="datum", required = false) LocalDate datum){
        if (datum == null) {
            datum = LocalDate.now();
        }

        KursTyp result = kursverwaltungService.getKursTyp(id, datum);
        if(result == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(KursTypDTO.fromKursTyp(result));
    }

    @PostMapping(value = "/api/stammdaten/kurstypen", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<KursTypDTO> addKurstyp(@RequestBody KursTypDTO inputDTO, UriComponentsBuilder uriComponentsBuilder) {
        KursTyp inputKursTyp = inputDTO.toKursTyp();
        KursTyp newKursTyp = this.kursverwaltungService.addKursTyp(inputKursTyp);
        KursTypDTO newDTO = KursTypDTO.fromKursTyp(newKursTyp);

        UriComponents uriComponents = uriComponentsBuilder.path("/api/stammdaten/kurstypen/{id}").buildAndExpand(newDTO.getId()).normalize();

        return ResponseEntity.created(uriComponents.toUri()).body(newDTO);
    }

    @PatchMapping(value = "/api/stammdaten/kurstypen/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<KursTypDTO> updateKurstyp(@RequestBody KursTypDTO inputDTO, @PathVariable UUID id) {
        KursTyp inputKursTyp = inputDTO.toKursTyp();
        if (!inputKursTyp.id().equals(id)) {
            ProblemDetail problemDetail = ProblemDetail.forStatus(400);
            problemDetail.setDetail("ID in URI passt nicht zur ID im Request-Body");
            problemDetail.setTitle("IDs passen nicht überein");
            return ResponseEntity.of(problemDetail).build();
        }

        KursTyp updatedKursTyp = this.kursverwaltungService.updateKursTyp(inputKursTyp);
        if(updatedKursTyp == null){
            return ResponseEntity.notFound().build();
        }

        KursTypDTO updatedDTO = KursTypDTO.fromKursTyp(updatedKursTyp);
        return ResponseEntity.ok(updatedDTO);
    }
}
