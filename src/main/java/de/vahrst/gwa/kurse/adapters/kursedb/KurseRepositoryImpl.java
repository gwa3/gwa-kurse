package de.vahrst.gwa.kurse.adapters.kursedb;

import de.vahrst.gwa.kurse.domain.ports.KurseRepository;
import de.vahrst.gwa.kurse.domain.ports.types.KursTyp;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Service
public class KurseRepositoryImpl implements KurseRepository {

    /**
     * Referenz auf das Respository für die KursTypen
     */
    private final KursTypenRepository kursTypenRepository;

    public KurseRepositoryImpl(KursTypenRepository kursTypenRepository) {
        this.kursTypenRepository = kursTypenRepository;
    }

    /**
     * liefert alle gespeicherten Kurstypen
     *
     * @return die Liste der gespeicherten Kurstypen
     */
    @Override
    public List<KursTyp> getKursTypen() {
        return kursTypenRepository.getKursTypen();
    }

    /**
     * liefert den Kurstyp für die vorgegebene ID und das vorgegebene Datum
     *
     * @param id    Die Id des gesuchten KursTyps
     * @param datum das Sicht-Datum
     * @return der gefundene KursTyp oder null, wenn es zur ID keinen KursTyp gibt oder der KursTyp nicht mehr gültig ist.
     */
    public KursTyp getKursTyp(UUID id, LocalDate datum){
        return kursTypenRepository.getKursTyp(id, datum);
    }


    /**
     * fügt den vorgegebenen KursTyp in die Liste der Kurstypen ein.
     * Das resultierende Objekt enthält die vergebenen UUID
     *
     * @param kursTyp der hinzuzufügende KursTyp
     * @return KursTyp mit den gespeicherten Daten, incl. der vergebenen UUID
     */
    @Override
    public KursTyp addKursTyp(KursTyp kursTyp) {
        return kursTypenRepository.insertKursTyp(kursTyp);
    }

    /**
     * aktualisiert den vorgegebenen KursTyp (identifiziert über die ID) mit den Daten des KursTyps
     * Wenn zur ID kein KursTyp vorhanden ist, wird null zurückgegeben.
     *
     * @param kursTyp der zu aktualisierende KursTyp
     * @return KursTyp mit den gespeicherten Daten
     */
    @Override
    @Nullable
    public KursTyp updateKursTyp(KursTyp kursTyp) {
        return kursTypenRepository.updateKursTyp(kursTyp);
    }

}
