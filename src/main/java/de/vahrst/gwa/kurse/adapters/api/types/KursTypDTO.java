package de.vahrst.gwa.kurse.adapters.api.types;

import de.vahrst.gwa.kurse.domain.ports.types.KursTyp;

import java.time.LocalDate;
import java.util.UUID;

/**
 * Web-Datenobjekt, das eine Instanz eines KursTyps repräsentiert.
 */
public class KursTypDTO {
    private String id;
    private String name;
    private String bezeichnung;
    private LocalDate gueltigBis;

    /**
     * erzeugt eine DTO-Instanz anhand des vorgegebenen Domänen-Typs
     *
     * @param kursTyp
     * @return
     */
    public static KursTypDTO fromKursTyp(KursTyp kursTyp) {
        if (kursTyp == null) {
            return null;
        }
        KursTypDTO dto = new KursTypDTO();
        if(kursTyp.id() != null) {
            dto.id = kursTyp.id().toString();
        }
        dto.name = kursTyp.name();
        dto.bezeichnung = kursTyp.beschreibung();
        dto.gueltigBis = kursTyp.gueltigBis();
        return dto;
    }

    /**
     * erzeugt aus diesem DTO ein Domänen-Objekt
     */
    public KursTyp toKursTyp(){
        UUID uuid = id == null ? null : UUID.fromString(id);
        return new KursTyp(uuid, name, bezeichnung, gueltigBis);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    public LocalDate getGueltigBis() {
        return gueltigBis;
    }

    public void setGueltigBis(LocalDate gueltigBis) {
        this.gueltigBis = gueltigBis;
    }
}
