package de.vahrst.gwa.kurse.adapters.kursedb;

import de.vahrst.gwa.kurse.domain.ports.types.KursTyp;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.JdbcUpdateAffectedIncorrectNumberOfRowsException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Service
public class KursTypenRepository {

    private static final String SELECT_KURS_TYP_SQL = """
            select id, name, beschreibung, gueltig_bis
            from kurs_typen
            where id = :id 
            and gueltig_bis >= :gueltigBis;
            """;

    private static final String SELECT_KURS_TYPEN_SQL = """
            select id, name, beschreibung, gueltig_bis 
            from kurs_typen;
            """;

    private static final String INSERT_KURS_TYP_SQL = """
            insert into kurs_typen (id, name, beschreibung, gueltig_bis)
            values(:id, :name, :beschreibung, :gueltigBis)
            """;

    private static final String UPDATE_KURS_TYP_SQL = """
            update kurs_typen 
            set name=:name, beschreibung=:beschreibung, gueltig_bis=:gueltigBis
            where id=:id
            """;

    private final NamedParameterJdbcTemplate jdbcTemplate;

    private final RowMapper<KursTyp> rowMapper = new KursTypenRowMapper();

    public KursTypenRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * liefert alle gespeicherten Kurstypen
     *
     * @return die Liste der gespeicherten Kurstypen
     */
    public List<KursTyp> getKursTypen() {
        return jdbcTemplate.query(SELECT_KURS_TYPEN_SQL, rowMapper);
    }

    /**
     * fügt den vorgegebenen KursTyp in die Datenbank ein. Vergibt eine
     * neue UUID für den KursTyp und liefert das angelegte Objekt (inkl. Id)
     * zurück.
     *
     * @param input der zu speichernde KursTyp
     * @return
     */
    public KursTyp insertKursTyp(KursTyp input) {
        UUID id = UUID.randomUUID();
        MapSqlParameterSource params = new MapSqlParameterSource()
                .addValue("id", id)
                .addValue("name", input.name())
                .addValue("beschreibung", input.beschreibung())
                .addValue("gueltigBis", input.gueltigBis());
        int update = jdbcTemplate.update(INSERT_KURS_TYP_SQL, params);
        if (update == 1) {
            return new KursTyp(id, input.name(), input.beschreibung(), input.gueltigBis());
        } else {
            throw new JdbcUpdateAffectedIncorrectNumberOfRowsException(INSERT_KURS_TYP_SQL, 1, update);
        }
    }

    /**
     * aktualisiert den vorgegebenen KursTyp (identifiziert über die ID) mit den Daten des KursTyps.
     * Wenn zur ID kein KursTyp vorhanden ist, wird null zurückgegeben.
     *
     * @param input der zu aktualisierende KursTyp
     * @return KursTyp mit den gespeicherten Daten
     */
    @Nullable
    public KursTyp updateKursTyp(KursTyp input) {
        MapSqlParameterSource params = new MapSqlParameterSource()
                .addValue("id", input.id())
                .addValue("name", input.name())
                .addValue("beschreibung", input.beschreibung())
                .addValue("gueltigBis", input.gueltigBis());

        int update = jdbcTemplate.update(UPDATE_KURS_TYP_SQL, params);

        return switch (update) {
            case 0 -> null;  // nicht gefunden.
            case 1 -> new KursTyp(input.id(), input.name(), input.beschreibung(), input.gueltigBis());
            default -> throw new JdbcUpdateAffectedIncorrectNumberOfRowsException(INSERT_KURS_TYP_SQL, 1, update);
        };

    }

    /**
     * liefert den Kurstyp für die vorgegebene ID und das vorgegebene Datum
     *
     * @param id    Die Id des gesuchten KursTyps
     * @param datum das Sicht-Datum
     * @return der gefundene KursTyp oder null, wenn es zur ID keinen KursTyp gibt oder der KursTyp nicht mehr gültig ist.
     */
    public KursTyp getKursTyp(UUID id, LocalDate datum) {
        MapSqlParameterSource params = new MapSqlParameterSource()
                .addValue("id", id)
                .addValue("gueltigBis", datum);

        try {
            return jdbcTemplate.queryForObject(SELECT_KURS_TYP_SQL, params, rowMapper);
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }


    private static class KursTypenRowMapper implements RowMapper<KursTyp> {

        @Override
        public KursTyp mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new KursTyp(
                    rs.getObject("id", UUID.class),
                    rs.getString("name"),
                    rs.getString("beschreibung"),
                    rs.getDate("gueltig_bis").toLocalDate()
            );
        }
    }
}
