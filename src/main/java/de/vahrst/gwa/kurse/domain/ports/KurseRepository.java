package de.vahrst.gwa.kurse.domain.ports;

import de.vahrst.gwa.kurse.domain.ports.types.KursTyp;
import org.springframework.lang.Nullable;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

public interface KurseRepository {


    /**
     * liefert den Kurstyp für die vorgegebene ID und das vorgegebene Datum
     *
     * @param id    Die Id des gesuchten KursTyps
     * @param datum das Sicht-Datum
     * @return der gefundene KursTyp oder null, wenn es zur ID keinen KursTyp gibt oder der KursTyp nicht mehr gültig ist.
     */
    KursTyp getKursTyp(UUID id, LocalDate datum);

    /**
     * liefert alle gespeicherten Kurstypen
     *
     * @return die Liste der gespeicherten Kurstypen
     */
    List<KursTyp> getKursTypen();

    /**
     * fügt den vorgegebenen KursTyp in die Liste der Kurstypen ein.
     * Das resultierende Objekt enthält die vergebenen UUID
     *
     * @param kursTyp der hinzuzufügende KursTyp
     * @return KursTyp mit den gespeicherten Daten, incl. der vergebenen UUID
     */
    KursTyp addKursTyp(KursTyp kursTyp);

    /**
     * aktualisiert den vorgegebenen KursTyp (identifiziert über die ID) mit den Daten des KursTyps
     * Wenn zur ID kein KursTyp vorhanden ist, wird null zurückgegeben.
     *
     * @param kursTyp der zu aktualisierende KursTyp
     * @return KursTyp mit den gespeicherten Daten
     */
    @Nullable
    KursTyp updateKursTyp(KursTyp kursTyp) ;


}
