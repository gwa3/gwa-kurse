package de.vahrst.gwa.kurse.domain.ports;

import de.vahrst.gwa.kurse.domain.ports.types.KursTyp;
import org.springframework.lang.Nullable;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

public interface KursverwaltungService {

    /**
     * liefert den Kurstyp für die vorgegebene ID und das vorgegebene Datum
     *
     * @param id    Die Id des gesuchten KursTyps
     * @param datum das Sicht-Datum
     * @return der gefundene KursTyp oder null, wenn es zur ID keinen KursTyp gibt oder der KursTyp nicht mehr gültig ist.
     */
    KursTyp getKursTyp(UUID id, LocalDate datum);


    /**
     * Liefert alle verfügbaren Kurstypen, die am vorgegebenen Datum gültig sind.
     *
     * @param datum das Sicht-Datum
     * @return Liste der gültigen KursTypen
     */
    List<KursTyp> getKursTypen(LocalDate datum);

    /**
     * liefert alle verfügbaren Kurstypen, die *heute* gültig sind
     *
     * @return die Liste der passenden Kurstypen
     */
    List<KursTyp> getKursTypen();

    /**
     * fügt den vorgegebenen Kurstyp in die interne Liste der Kurstypen ein.
     *
     * @param input der hinzuzufügende Kurstyp, id muss null sein
     * @return der resultierende KursTyp, mit der neu vergebenen ID
     */
    KursTyp addKursTyp(KursTyp input);

    /**
     * aktualisiert den vorgegebenen KursTyp (identifiziert über die ID) mit den Daten des KursTyps
     * Wenn zur ID kein KursTyp vorhanden ist, wird null zurückgegeben.
     *
     * @param kursTyp der zu aktualisierende KursTyp
     * @return KursTyp mit den gespeicherten Daten
     */
    @Nullable
    KursTyp updateKursTyp(KursTyp kursTyp) ;


}
