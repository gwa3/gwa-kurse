package de.vahrst.gwa.kurse.domain.model;

import de.vahrst.gwa.kurse.domain.ports.KurseRepository;
import de.vahrst.gwa.kurse.domain.ports.KursverwaltungService;
import de.vahrst.gwa.kurse.domain.ports.types.KursTyp;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Service
public class KursverwaltungServiceImpl implements KursverwaltungService {
    private final KurseRepository kurseRepository;

    /**
     * Konstruktor
     *
     * @param kurseRepository Referenz auf das Kurse-Repository
     */
    public KursverwaltungServiceImpl(KurseRepository kurseRepository) {
        this.kurseRepository = kurseRepository;
    }

    /**
     * liefert alle verfügbaren Kurstypen, die am vorgegebenen Datum gültig sind.
     *
     * @param datum das Sicht-Datum
     * @return Liste der gültigen KursTypen
     */
    @Override
    public List<KursTyp> getKursTypen(@NonNull LocalDate datum) {
        Assert.notNull(datum, "Datum-Parameter darf nicht null sein!");

        List<KursTyp> kursTypen = kurseRepository.getKursTypen();
        return kursTypen.stream()
                .filter(kursTyp -> !datum.isAfter(kursTyp.gueltigBis()))
                .toList();
    }

    /**
     * liefert den Kurstyp für die vorgegebene ID und das vorgegebene Datum
     *
     * @param id    Die Id des gesuchten KursTyps
     * @param datum das Sicht-Datum
     * @return der gefundene KursTyp oder null, wenn es zur ID keinen KursTyp gibt oder der KursTyp nicht mehr gültig ist.
     */
    @Override
    public KursTyp getKursTyp(UUID id, LocalDate datum){
        Assert.notNull(id, "id darf nicht null sein!");
        Assert.notNull(datum, "Datum-Parameter darf nicht null sein!");

        return kurseRepository.getKursTyp(id, datum);
    }

    /**
     * liefert alle verfügbaren Kurstypen, die *heute* gültig sind
     *
     * @return
     */
    @Override
    public List<KursTyp> getKursTypen() {
        return getKursTypen(LocalDate.now());
    }

    /**
     * fügt den vorgegebenen KursTyp in die Datenbank ein. Vergibt eine
     * neue UUID für den KursTyp und liefert das angelegte Objekt (inkl. Id)
     * zurück.
     *
     * @param input der zu speichernde KursTyp
     * @return der resultierende KursTyp incl. Id.
     */
    @Override
    public KursTyp addKursTyp(KursTyp input) {
        return kurseRepository.addKursTyp(input);
    }

    /**
     * aktualisiert den vorgegebenen KursTyp (identifiziert über die ID) mit den Daten des KursTyps
     * Wenn zur ID kein KursTyp vorhanden ist, wird null zurückgegeben.
     *
     * @param kursTyp der zu aktualisierende KursTyp
     * @return KursTyp mit den gespeicherten Daten
     */
    @Override
    @Nullable
    public KursTyp updateKursTyp(KursTyp kursTyp){
        return kurseRepository.updateKursTyp(kursTyp);
    }

}