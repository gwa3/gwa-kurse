package de.vahrst.gwa.kurse.domain.ports.types;

import java.time.LocalDate;
import java.util.UUID;

/**
 * Repräsentiert einen Kurs-Typ, wie z.B. Indoorcycling, Schlingentraining etc.
 * @param id
 * @param name
 * @param beschreibung
 */
public record KursTyp(UUID id, String name, String beschreibung, LocalDate gueltigBis) {

}
