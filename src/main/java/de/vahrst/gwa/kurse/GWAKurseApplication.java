package de.vahrst.gwa.kurse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GWAKurseApplication {

	public static void main(String[] args) {
		SpringApplication.run(GWAKurseApplication.class, args);
	}

}
